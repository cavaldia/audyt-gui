﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WpfCharts;
using System.Collections.ObjectModel;
using AutydGUI.Models;
using System.ComponentModel;
using AutydGUI.Tabs;
using System.IO;

namespace AutydGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public string[] Values_Axes { get; set; }
        public ObservableCollection<ChartLine> Values_Lines { get; set; }
        public Models.Questions CurrentQuestion;

        private List<Models.Domain> ListOfMainClass;
        private List<Models.Questions> ListOfQuestions;
        private ObservableCollection<Values_GridView> values_GridView;
        public ObservableCollection<Values_GridView> _Values_GridView
        {
            get
            {
                return this.values_GridView;
            }
            set
            {
                this.values_GridView = value;
                //this.NotifyPropertyChanged(_Values_GridView);
            }
         }

        public MainWindow()
        {
            InitializeComponent();
            #region Audit_Tab_Init
            InitializeListOfMainClass(ref ListOfMainClass, ref ListOfQuestions);
            if( ListOfMainClass != null)
            {
                InitializeTreeViewForAudit(ListOfMainClass);
            }
            GUI_Audit_Slider.Maximum = ListOfQuestions.Count-1;
            if(ListOfQuestions.Count > 0)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView(0);
            }
#endregion
        }

#region Audit_Tab
        void InitializeListOfMainClass(ref List<Models.Domain> _ListOfMainClass, ref List<Models.Questions> _ListOfQuestions)
        {
            try
            {
                // Reading file to string matrix
                StreamReader SR = new StreamReader(Environment.CurrentDirectory + "/CSV/Audit_Questions.csv", Encoding.UTF8);  //hardcoding FTW
                string input = SR.ReadToEnd();
                string[][] linesMatrix = input.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Split(new[] { ';' }, StringSplitOptions.None)).ToArray();

                // Lists initializing
                _ListOfMainClass = new List<Domain>();
                _ListOfQuestions = new List<Questions>();

                // Creating List
                int mainClassIndex = 0;
                int subClassIndex = 0;
                int subSubClassIndex = 0;
                int questionIndex = 0;

                for (int i = 0; i < linesMatrix.GetLength(0); ++i )
                {
                    if (linesMatrix[i].Length >= 3)
                    {
                        if (linesMatrix[i][0] == String.Empty)
                        {
                            if (i + 1 < linesMatrix.GetLength(0) && linesMatrix[i + 1][0] == String.Empty)
                            {
                                if (i + 2 < linesMatrix.GetLength(0) && linesMatrix[i + 2][0] == String.Empty) // This is Domain
                                {
                                    subClassIndex = 0;
                                    Models.Domain temp = new Domain();
                                    temp.Title = linesMatrix[i][1];
                                    temp.ListOfServices = new List<Service>();
                                    _ListOfMainClass.Add(temp);
                                    ++mainClassIndex;
                                }
                                else // This is Service
                                {
                                    subSubClassIndex = 0;
                                    Models.Service temp = new Service();
                                    temp.Title = linesMatrix[i][1];
                                    temp.ListOfSubServices = new List<SubService>();
                                    _ListOfMainClass[mainClassIndex - 1].ListOfServices.Add(temp);
                                    ++subClassIndex;
                                }
                            }
                            else // this is SubServices
                            {
                                questionIndex = 0;
                                Models.SubService temp = new SubService();
                                temp.Title = linesMatrix[i][1];
                                temp.ListOfQuestions = new List<Questions>();
                                _ListOfMainClass[mainClassIndex - 1].ListOfServices[subClassIndex - 1].ListOfSubServices.Add(temp);
                                ++subSubClassIndex;
                            }
                        }
                        else // this is question
                        {
                            Models.Questions temp = new Questions();
                            
                            temp.ID = linesMatrix[i][0];
                            temp.Question = linesMatrix[i][1];
                            temp.Header = temp.Question;
                            temp.QuestionAnswer = Models.Answer.BrakOdp;

                            _ListOfMainClass[mainClassIndex - 1].ListOfServices[subClassIndex - 1].ListOfSubServices[subSubClassIndex - 1].ListOfQuestions.Add(temp);
                            _ListOfQuestions.Add(temp);
                            ++questionIndex;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void InitializeTreeViewForAudit( List<Models.Domain> _ListOfMainClass)
        {
            TreeViewItem treeItem_1 = null;
            TreeViewItem treeItem_2 = null;
            TreeViewItem treeItem_3 = null;
            for(int i = 0; i < _ListOfMainClass.Count; ++i)
            {
                treeItem_1 = new TreeViewItem() { Header = _ListOfMainClass[i].Title };
                for(int j = 0; j < _ListOfMainClass[i].ListOfServices.Count; ++j)
                {
                    treeItem_2 = new TreeViewItem() { Header = _ListOfMainClass[i].ListOfServices[j].Title };
                    for(int k = 0; k < _ListOfMainClass[i].ListOfServices[j].ListOfSubServices.Count; ++k)
                    {
                        treeItem_3 = new TreeViewItem() { Header = _ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].Title };
                        for (int x = 0; x < _ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].ListOfQuestions.Count; ++x )
                        {
                            treeItem_3.Items.Add(_ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].ListOfQuestions[x]);
                        }
                        treeItem_2.Items.Add(treeItem_3);
                    }
                    treeItem_1.Items.Add(treeItem_2);
                }
                GUI_Audit_QuestionsTreeView.Items.Add(treeItem_1);
            }
           
        }
        private void GUI_Audit_QuestionsTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if ((sender as TreeView).SelectedItem is Models.Questions)
            {
                CurrentQuestion = ((sender as TreeView).SelectedItem as Models.Questions);
                GUI_Audit_QuestionBox.Text = CurrentQuestion.Question;
                GUI_Audit_AnswersBox.Text = CurrentQuestion.Comment;
                GUI_Audit_SupportBox.Text = CurrentQuestion.Support;

                TreeViewItem parent = ((sender as TreeView).SelectedItem as TreeViewItem).Parent as TreeViewItem;
                if (parent != null)
                {
                    GUI_Audit_Subservice.Text = CurrentQuestion.ID + " - " + parent.Header;
                }

                int indexInList = 0;
                foreach (Models.Questions item in ListOfQuestions)
                {
                    if (item == CurrentQuestion)
                    {
                        GUI_Audit_Slider.Value = indexInList;
                    }
                    ++indexInList;
                }

                // Radio buttons:
                switch (CurrentQuestion.QuestionAnswer)
                {
                    case Models.Answer.Bezprzedmiotowo:
                        {
                            GUI_Audit_Radio_WTF.IsChecked = true;
                            break;
                        }
                    case Models.Answer.BrakOdp:
                        {
                            GUI_Audit_Radio_NoAnser.IsChecked = true;
                            break;
                        }
                    case Models.Answer.Nie:
                        {
                            GUI_Audit_Radio_No.IsChecked = true;
                            break;
                        }
                    case Models.Answer.Tak:
                        {
                            GUI_Audit_Radio_Yes.IsChecked = true;
                            break;
                        }
                }
            }
        }
        private void GUI_Audit_RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (CurrentQuestion != null)
            {
                if (GUI_Audit_Radio_Yes.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.Tak;
                }
                if (GUI_Audit_Radio_No.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.Nie;
                }
                if (GUI_Audit_Radio_NoAnser.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.BrakOdp;
                }
                if (GUI_Audit_Radio_WTF.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.Bezprzedmiotowo;
                }
            }
        }
        private void GUI_Audit_AnswersBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(CurrentQuestion != null)
            {
                CurrentQuestion.Comment = GUI_Audit_AnswersBox.Text;
            }
        }
        private void GUI_Audit_GoLeft_Click(object sender, RoutedEventArgs e)
        {
            if(CurrentQuestion != null)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView( (GUI_Audit_Slider.Value - 1 ) >= 0 ? (int)GUI_Audit_Slider.Value - 1:ListOfQuestions.Count-1  );
            }
        }
        private void GUI_Audit_GoRight_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentQuestion != null)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView(((int)GUI_Audit_Slider.Value+1 )% ListOfQuestions.Count);
            }
        }
        private void GUI_Audit_Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(CurrentQuestion != null)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView((int)GUI_Audit_Slider.Value);
            }
        }
        private void ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView(int number)
        {
            TreeViewItem temp = null;
            foreach (TreeViewItem Lay1 in GUI_Audit_QuestionsTreeView.Items)
            {
                Lay1.IsExpanded = false;
                foreach (TreeViewItem Lay2 in Lay1.Items)
                {
                    Lay2.IsExpanded = false;
                    foreach (TreeViewItem Lay3 in Lay2.Items)
                    {
                        Lay3.IsExpanded = false;
                        foreach (Models.Questions Lay4 in Lay3.Items)
                        {
                            Lay4.IsExpanded = false;
                            if (Lay4 == ListOfQuestions[number])
                            {
                                Lay4.IsExpanded = true;
                                Lay4.IsSelected = true;
                                temp = Lay4 as TreeViewItem;
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < 3; ++i)
            {
                temp = (temp.Parent as TreeViewItem);
                temp.IsExpanded = true;
            }
        }
#endregion

        private readonly Random random = new Random(666);

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ScrollBar_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }
        public List<double> GenerateRandomDataSet(int nmbrOfPoints)
        {
            var pts = new List<double>(nmbrOfPoints);
            for (var i = 0; i < nmbrOfPoints; i++)
            {
                pts.Add(random.NextDouble());
            }
            return pts;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = this;

            Values_Axes = new[] { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7" };

            Values_Lines = new ObservableCollection<ChartLine> {
                                                            new ChartLine {
                                                                              LineColor = Colors.Red,
                                                                              FillColor = Color.FromArgb(128, 255, 0, 0),
                                                                              LineThickness = 2,
                                                                              PointDataSource = GenerateRandomDataSet(Values_Axes.Length),
                                                                              Name = "Chart 1"
                                                                          },
                                                            new ChartLine {
                                                                              LineColor = Colors.Blue,
                                                                              FillColor = Color.FromArgb(128, 0, 0, 255),
                                                                              LineThickness = 2,
                                                                              PointDataSource = GenerateRandomDataSet(Values_Axes.Length),
                                                                              Name = "Chart 2"
                                                                          }
                                                        };

            _Values_GridView = new ObservableCollection<Values_GridView>() {
                new Values_GridView()
                {
                   Cell = "1", Field = "2", Title = "3333", Value_K = 2.3, Value_P = 1.3
                },
                new Values_GridView()
                {
                   Cell = "1", Field = "2", Title = "444", Value_K = 2.3, Value_P = 1.3
                },
                new Values_GridView()
                {
                   Cell = "4", Field = "2", Title = "444", Value_K = 2.3, Value_P = 1.3
                }
            };
        }

    }
}
