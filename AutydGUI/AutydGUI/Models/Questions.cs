﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Controls;

namespace AutydGUI.Models
{
    public enum Answer
    {
        Tak,
        Nie,
        [Description("Brak odpowiedzi")] BrakOdp,
        [Description("Bez przedmiotowo")] Bezprzedmiotowo

    }
    public class Questions : TreeViewItem
    {
        public string ID { get; set; }
        public string Question { get; set; }
        public Answer QuestionAnswer { get; set; }
        public string Comment { get; set; }
        public string Support { get; set; }
    }
    class Domain
    {
        public string Title { get; set; }
        public List<Service> ListOfServices { get; set; }
    }
    class Service
    {
        public string Title {get; set;}
        public List<SubService> ListOfSubServices { get; set; }
    }
    class SubService
    {
        public string Title { get; set; }
        public List<Questions> ListOfQuestions { get; set; }
    }

}
