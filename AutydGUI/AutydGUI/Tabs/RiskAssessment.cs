﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WpfCharts;
using System.Collections.ObjectModel;
using AutydGUI.Models;
using System.ComponentModel;

namespace AutydGUI.Tabs
{
    class RiskAssessment
    {
        public string[] RiskAssessment_Axes { get; set; }
        public ObservableCollection<ChartLine> RiskAssessment_Lines { get; set; }

        private void LoadRiskAssessmentChart()
        {
            RiskAssessment_Axes = new[] { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9" }; // finalnie ilosc grup wczytanych z pliku z danymi tak jak na screenie nr 11?

            RiskAssessment_Lines = new ObservableCollection<ChartLine> {
                new ChartLine {
                    LineColor = Colors.Red,
                    FillColor = Color.FromArgb(128, 255, 0, 0),
                    LineThickness = 2,
                    //PointDataSource = GenerateRandomDataSet(RiskAssessment_Axes.Length),
                    Name = "Chart 1"
                },
                new ChartLine {
                    LineColor = Colors.Blue,
                    FillColor = Color.FromArgb(128, 0, 0, 255),
                    LineThickness = 2,
                    //PointDataSource = GenerateRandomDataSet(RiskAssessment_Axes.Length),
                    Name = "Chart 2"
                }
            };
        }
    }
}
